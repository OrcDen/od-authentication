import { 
    CognitoIdentityProviderClient, 
    AdminCreateUserCommand, 
    ListUsersCommand, 
    AdminGetUserCommand, 
    AdminDeleteUserCommand, 
    AdminUpdateUserAttributesCommand
} from "@aws-sdk/client-cognito-identity-provider";
import { CognitoIdentityClient } from "@aws-sdk/client-cognito-identity";
import { fromCognitoIdentityPool } from "@aws-sdk/credential-provider-cognito-identity";

export class ODCognitoAdmin {  

    constructor( config, cognitoAuth ) {
        this._user = null;

        this._REGION = config.region;
        this._USER_POOL_ID = config.userPoolId;
        this._IDENTITY_POOL_ID = config.identityPoolId;

        this._cognitoAuth = cognitoAuth;
    }

    _createCognitoIdentityProviderClient() {
        let logins = {}
        logins['cognito-idp.' + this._REGION + '.amazonaws.com/' + this._USER_POOL_ID] = this.user.signInUserSession.idToken.getJwtToken();

        return new CognitoIdentityProviderClient(
            { 
                region: this._REGION,
                credentials: fromCognitoIdentityPool({
                    client: new CognitoIdentityClient({ region: this._REGION }),
                    identityPoolId: this._IDENTITY_POOL_ID,
                    logins: logins
                })
            }
        );
    }

    get user() {
        return this._cognitoAuth.user;
    }

    async adminCreateUser( userName, email, callback ) {
        if( !this.user || !this.user.signInUserSession ) {
            return callback( null, "ERROR: No Authenticated User" )
        }
        let cognitoidentityserviceprovider = this._createCognitoIdentityProviderClient();
        var params = {
            UserPoolId: this._USER_POOL_ID,
            Username: userName,
            UserAttributes: [
              {
                Name: 'email',
                Value: email
              },
              {
                Name: 'email_verified',
                Value: 'true'
              }
            ]
        };
        const command = new AdminCreateUserCommand( params );
          
        let err = null;
        let data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to create the user: ', error );
            err = error;
        }
        if( callback ) {
            callback( data, err );
        }
        return data;
    }

    async adminListUsers( filter, pagination, callback ) {
        if( !this.user || !this.user.signInUserSession ) {
            return callback( null, "ERROR: No Authenticated User" )
        }
        let cognitoidentityserviceprovider = this._createCognitoIdentityProviderClient();
        var params = {
            UserPoolId: this._USER_POOL_ID
        };
        if( filter ) {
            params['Filter'] = filter;
        }
        if( pagination ) {
            params['PaginationToken'] = pagination;
        }
        const command = new ListUsersCommand( params );
          
        let err = null;
        let data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to list all user pool users: ', error );
            err = error;
        }
        if( callback ) {
            callback( data.Users, err );
        }
        return data.Users;
    }

    async adminGetUser( userName, callback ) {
        if( !this.user || !this.user.signInUserSession ) {
            return callback( null, "ERROR: No Authenticated User" )
        }
        let cognitoidentityserviceprovider = this._createCognitoIdentityProviderClient();
        var params = {
            UserPoolId: this._USER_POOL_ID,
            Username: userName
        };
        const command = new AdminGetUserCommand( params );
          
        let err = null;
        let data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to get the user: ', error );
            err = error;
        }
        if( callback ) {
            callback( data, err );
        }
        return data;
    }

    async adminGetUserByEmail( email, callback ) {
        if( !this.user || !this.user.signInUserSession ) {
            return callback( null, "ERROR: No Authenticated User" )
        }
        let cognitoidentityserviceprovider = this._createCognitoIdentityProviderClient();
        var params = {
            UserPoolId: this._USER_POOL_ID,
            Filter: 'email = "' + email + '"'
        };
        const command = new ListUsersCommand( params );
          
        let err = null;
        let data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to get the user: ', error );
            err = error;
        }
        let returnVal = null;
        if( data.Users && data.Users.length && data.Users.length > 0 ) {
            if( data.Users.length > 1 ) {
                err = "Multiple users found with the same email."
            } else {
                returnVal = data.Users[0];
            }
        }
        if( callback ) {
            callback( returnVal, err );
        }
        return returnVal;
    }

    async adminDeleteUser( userName, callback ) {
        if( !this.user || !this.user.signInUserSession ) {
            return callback( null, "ERROR: No Authenticated User" )
        }
        let cognitoidentityserviceprovider = this._createCognitoIdentityProviderClient();
        var params = {
            UserPoolId: this._USER_POOL_ID,
            Username: userName
        };
        const command = new AdminDeleteUserCommand( params );
          
        let err = null;
        let data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to delete the user: ', error );
            err = error;
        }
        if( callback ) {
            callback( data, err );
        }
        return data;
    }

    async adminUpdateUserEmail( userName, email, callback ) {
        if( !this.user || !this.user.signInUserSession ) {
            return callback( null, "ERROR: No Authenticated User" )
        }
        let cognitoidentityserviceprovider = this._createCognitoIdentityProviderClient();
        var params = {
            UserPoolId: this._USER_POOL_ID,
            Username: userName,
            UserAttributes: [
                {
                    Name: 'email',
                    Value: email
                  },
                  {
                    Name: 'email_verified',
                    Value: 'true'
                  }
            ]
        };
        const command = new AdminUpdateUserAttributesCommand( params );
          
        let err = null;
        let data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to update the users email: ', error );
            err = error;
        }
        if( callback ) {
            callback( data, err );
        }
        return data;
    }

    async adminResendInvites( userName, newUserName, email, callback ) {
        if( !this.user || !this.user.signInUserSession ) {
            return callback( null, "ERROR: No Authenticated User" )
        }
        let cognitoidentityserviceprovider = this._createCognitoIdentityProviderClient();

        //Delete User
        var params = {
            UserPoolId: this._USER_POOL_ID,
            Username: userName,
        };
        let command = new AdminDeleteUserCommand( params );          
        let err = null;
        let data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to resend the invite emails: ', error );
            err = error;
            return;
        }

        //Re-Create User
        params = {
            UserPoolId: this._USER_POOL_ID,
            Username: newUserName,
            UserAttributes: [
              {
                Name: 'email',
                Value: email
              },
              {
                Name: 'email_verified',
                Value: 'true'
              }
            ]
        };
        command = new AdminCreateUserCommand( params );          
        err = null;
        data = null;
        try {
            data = await cognitoidentityserviceprovider.send( command );
        } catch( error ) {
            console.log( 'There was an error while trying to resend the invite emails: ', error );
            err = error;
        }

        if( callback ) {
            callback( data, err );
        }
        return data;
    }
} 
