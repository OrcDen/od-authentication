# OD-Authentication

> Authentication Library

`ODCognitoAuth` is a simple library that will manage your connection to AWS Cognito Servers

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-authentication
```
## Usage - ODCognitoAuth
```
import '@orcden/od-authentication';
```
```
let auth = new ODCognitoAuth( this._getCognitoConfigs() );

_getCognitoConfigs() {
    return {
        // REQUIRED - Amazon Cognito Region
        region: process.env.APP_COGNITO_REGION,
        // OPTIONAL - Amazon Cognito Federated Identity Pool Region 
        // Required only if it's different from Amazon Cognito Region
        identityPoolRegion: process.env.APP_COGNITO_REGION,
        // OPTIONAL - Amazon Cognito User Pool ID
        userPoolId: process.env.APP_COGNITO_USER_POOL_ID ,
        // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
        userPoolWebClientId: process.env.APP_COGNITO_USER_POOL_WEB_CLIENT_ID,                        
        // OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
        authenticationFlowType: process.env.APP_COGNITO_AUTHENTICATION_FLOW_TYPE,
        // OPTIONAL - Hosted UI configuration
        oauth: {
            domain: null,
            scope: [],
            redirectSignIn: null,
            redirectSignOut: null,
            responseType: 'code' // or 'token', note that REFRESH token will only be generated when the responseType is code
        },
        // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
        identityPoolId: process.env.APP_COGNITO_IDENTITY_POOL_ID
    }
}
```

## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `tryGetCurrentAuthenticatedUser`   | callback | Trys to authenticates a user from session data and performs a callback with the info. Callback parameters: loggedIn, userDetails, error |
| `trySignIn`   | username, password, callback | Trys to manually authenticate with the given parameters and performs a callback with the info. Callback parameters: userDetails, challenge, error |
| `tryGetCurrenSessionData`   | callback | Gets a user from session data and performs a callback with the info. Needs to be authenticated first. Callback parameters: userDetails, error |
| `trySignUp`   | username, email, password, callback | Trys to sign a user up with the given parameters and performs a callback with the info. Callback parameters: userDetails, error |
| `tryCompleteNewPassword`   | newPassword, callback | Trys to complete a new password challenge with the given parameters and performs a callback with the info. Callback parameters: userDetails, error |
| `tryResendConfirmationCode`   | username, callback | Trys to resend a confirmation code to a specific user and performs a callback with the info. Callback parameters: error |
| `trySignOut`   | callback | Trys to sign out the current authenticated user and performs a callback with the info. Callback parameters: error |
| `tryChangePassword`   | oldPassword, newPassword, callback | Trys to change the password of the current authernticated user and performs a callback with the info. Callback parameters: passwordChangeData, error |
| `tryVerifyEmailSendCode`   | callback | Trys to send a code to the current authernticated user and performs a callback with the info. Callback parameters: error |
| `tryUpdateEmailSendCode`   | email, callback | Trys to update the email of and send a code to the current authernticated user and performs a callback with the info. Callback parameters: error |
| `tryVerifyEmailSubmit`   | code, callback | Trys to verify the email update code of current authernticated user and performs a callback with the info. Callback parameters: error |
| `tryforgotPasswordSendCode`   | username, callback | Trys to reset the password of and send a code to a specific user and performs a callback with the info. Callback parameters: error |
| `tryforgotPasswordSubmit`   | username, code, newPassword, callback | Trys to verify the email update code of a specific user to update their password and performs a callback with the info. Callback parameters: passwordChangeData, error |

## Development
### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```