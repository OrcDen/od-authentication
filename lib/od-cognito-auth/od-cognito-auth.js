import Amplify from '@aws-amplify/core';
import Auth from '@aws-amplify/auth';

export class ODCognitoAuth {  

    constructor( config ) {
        this._user = null;

        this._REGION = config.region;
        this._USER_POOL_ID = config.userPoolId;
        this._IDENTITY_POOL_ID = config.identityPoolId;

        //configure using static files
        Amplify.configure({
            Auth: {
        
                // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
                identityPoolId: this._IDENTITY_POOL_ID,
        
                // REQUIRED - Amazon Cognito Region
                region: this._REGION,
        
                // OPTIONAL - Amazon Cognito Federated Identity Pool Region 
                // Required only if it's different from Amazon Cognito Region
                identityPoolRegion: config.identityPoolRegion,
        
                // OPTIONAL - Amazon Cognito User Pool ID
                userPoolId: this._USER_POOL_ID,
        
                // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
                userPoolWebClientId: config.userPoolWebClientId,                        
                // OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
                authenticationFlowType: config.authenticationFlowType,
                // OPTIONAL - Hosted UI configuration
                oauth: {
                    domain: config.oauth.domain,
                    scope: config.oauth.scope,
                    redirectSignIn: config.oauth.redirectSignIn,
                    redirectSignOut: config.oauth.redirectSignOut,
                    responseType: config.oauth.responseType // or 'token', note that REFRESH token will only be generated when the responseType is code
                }

            }
        });
    }

    get user() {
        return this._user;
    }

    async tryGetCurrentAuthenticatedUser( callback ) {
        let loggedIn = false
        let err = null;
        try {
            this._user = await Auth.currentAuthenticatedUser({
                bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
            });
            loggedIn = true;
        } catch( error ) {
            console.log( 'No currently logged in user: ', error );
            err = error;
        }
        if( callback ) {
            callback( loggedIn, this.user, err );
        }
        return this.user;
    }

    async tryGetCurrenSessionData( callback ) {
        let err = null;
        let currentSessionData = null
        try {
            currentSessionData = await Auth.currentSession();
        } catch( error ) {
            console.log( 'There was an error while trying to retrieve the current session data: ', error );
            err = error;
        }
        if( callback ) {
            callback( currentSessionData, err );
        }
        return currentSessionData;
            
    }

    async trySignUp( username, email, password, callback ) {
        let err = null;
        try {
            this._user = await Auth.signUp({
                username,
                password,
                attributes: {
                    email 
                }
            });
        } catch( error ) {
            err = error;
            console.log( 'There was an error while trying to sign up:', error );
        }
        if( callback ) {
            callback( this.user, err );
        }
        return this.user;
    }

    async trySignIn( username, password, callback ) {
        let challenge = null;
        let err = null;
        try {
            this._user = await Auth.signIn( username, password );
            if( this.user.challengeName ) {
                challenge = this.user.challengeName;
            }
        } catch( error ) {
            err = error;
            console.log( 'There was an error while trying to sign in', error );
        }
        if( callback ) {
            callback( this.user, challenge, err );
        }
        return this.user;
    }

    async tryforgotPasswordSendCode( username, callback ) {
        let err = null
        try {
            await Auth.forgotPassword( username );
        } catch( error ) {
            err = error;
            console.log( error );
        }
        if( callback ) {
            callback( err );
        }
    }

    async tryResendConfirmationCode( username, callback ) {
        let err = null;
        try {
            await Auth.resendSignUp( username );
        } catch ( error ) {
            err = error;
            console.log('There was an error while trying to resend code: ', error );
        }        
        if( callback ) {
            callback( err );
        }
    }

    async tryforgotPasswordSubmit( username, code, newPassword, callback ) {
        let passwordChangeData = null;
        let err = null
        try {                   
            passwordChangeData = await Auth.forgotPasswordSubmit( username, code, newPassword );
        } catch( error ) {
            console.log( 'There was an error while trying to confirm new password: ', error );
            err = error;
        }
        if( callback ) {
            callback( passwordChangeData, err ); 
        }
        return passwordChangeData;           
    }

    async tryChangePassword( oldPassword, newPassword, callback ) {
        let passwordChangeData = null;
        let err = null
        try {
            passwordChangeData = await Auth.changePassword( this.user, oldPassword, newPassword );                    
        } catch( error ) {
            err = error;
            console.log('There was an error while trying to change password: ', error );
        }        
        if( callback ) {
            callback( passwordChangeData, err );
        }
        return passwordChangeData;
    }

    async tryCompleteNewPassword( newPassword, callback ) { //For FIRST Login challenge
        let err = null;
        try {
            this._user = await Auth.completeNewPassword(
                this.user,               // the Cognito User Object
                newPassword
            );
        } catch( error ) {
            console.log( 'There was an error while trying to complete new password' );
            err = error;
        }
        if( callback ) {
            callback( this.user, err );
        }
        return this.user;
    }

    async trySignOut( callback ) {
        let err = null;
        try {
            await Auth.signOut();
        } catch( error ) {
            err = error;
            console.log('There was an error while trying to sign out: ', error );
        }
        if( callback ) {
            callback( err );
        }
    }

    async tryVerifyEmailSendCode( callback ) {
        let err = null
        try {
            Auth.verifyCurrentUserAttribute( 'email' );
        } catch( error ) {
            console.log( 'There was an error while trying to send the email verification code: ', error );
            err = error;
        }
        if( callback ) {
            callback( err );
        }
    }

    async tryVerifyEmailSubmit( verificationCode, callback ) {
        let err = null
        try {
            Auth.verifyCurrentUserAttributeSubmit( 'email', verificationCode );
        } catch( error ) {
            console.log('There was an error while trying to verify email: ', error );
            err = error;
        }
        if( callback ) {
            callback( err );
        }
    }

    async tryUpdateEmailSendCode( email, callback ) {
        let err = null;
        try {
            await Auth.updateUserAttributes( this.user, {
                'email': email,
            });
        } catch( error ) {
            console.log( 'There was an error while trying to send update email code: ', error );
            err = error;
        }
        if( callback ) {
            callback( err );
        }
    }
} 
